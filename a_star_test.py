#!/usr/bin/env python
# -*- coding: utf-8 -*-


FREE_BLOCK = 0
GOAL_BLOCK = 2
WALL_BLOCK = 1
USER_BLOCK = 3


class Node(object):
    def __init__(self, position, g=0, h=0, parent=None):
        super(Node, self).__init__()
        self.parent = parent
        self.position = position
        self.g = g
        self.h = h
        self.f = g + h

    def set_parent(self, parent):
        self.parent = parent

    def __eq__(self, other):
        return self.position == other.position

    def __repr__(self):
        return '{cls_}(position={obj.position}, g={obj.g}, h={obj.h}, parent={obj.parent})'.format(
            cls_=self.__class__.__name__,
            obj=self,
        )


def is_walkable(node, graph):
    return graph[node.position[0]][node.position[1]] in (FREE_BLOCK, GOAL_BLOCK, USER_BLOCK)


def get_children(node, graph):
    row, column = node.position

    for row_offset in range(-1, 2):
        for column_offset in range(-1, 2):
            node_row = row + row_offset
            node_column = column + column_offset

            try:
                graph[node_row][node_column]
                in_graph = True
            except IndexError:
                in_graph = False

            is_self = node_row == row and node_column == column

            if in_graph and not is_self:
                yield Node((node_row, node_column), parent=node)


def get_end(graph):
    return get_first_index(graph, GOAL_BLOCK)


def get_start(graph):
    return get_first_index(graph, USER_BLOCK)


def get_first_index(graph, value):
    for row, line in enumerate(graph):
        for column, item in enumerate(line):
            if item == value:
                return (row, column)

    return (-1, -1)


def get_h(node, reference):
    return ((node.position[0] - reference.position[0]) ** 2) + ((node.position[1] - reference.position[1]) ** 2)


def trace(node):
    parent = node.parent

    path = []
    while parent:
        path.append(parent)
        parent = parent.parent

    return path


def a_star(graph, user, goal):
    open_list = [user]
    closed_list = set()

    while open_list:
        current_node = open_list[0]
        current_index = 0

        for index, node in enumerate(open_list):
            if node.f < current_node.f:
                current_node = node
                current_index = index

        if current_node == goal:
            goal.set_parent(current_node)
            path = trace(goal)
            return list(reversed(path))

        open_list.pop(current_index)
        closed_list.add(current_node)

        for child in get_children(current_node, graph):
            # TODO : Why does `__eq__` not work?
            if child in closed_list:
                continue

            if not is_walkable(child, graph):
                continue

            child.g = child.parent.g + 1
            child.h = get_h(child, goal)
            child.f = child.g + child.h

            for open_node in open_list:
                if child == open_node and child.g > open_node.g:
                    continue

            open_list.append(child)


def main():
    '''Run the main execution of the current script.'''
    graph = [
        [0, 0, 0, 0, 1, 0, 0],
        [0, 3, 0, 0, 1, 0, 2],
        [0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0, 0],
    ]

    user = Node(get_start(graph), parent=None)
    goal = Node(get_end(graph), parent=None)

    positions = [node.position for node in a_star(graph, user, goal)]
    print('path', positions)


if __name__ == '__main__':
    main()
